# -*- coding: utf-8 -*-
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from apps.jornal.models import Jornal
from django.db.models import Q
from datetime import datetime

def jornal(request, slug='', id=''):
    if slug!='' and id!='':
        jornal = get_object_or_404(Jornal, slug = slug,id=id, status = True)
    else:
        jornal = Jornal.objects.filter(status = True).order_by('-dt_edicao')[0]

    return render_to_response('capitalteresina/jornal/jornal.html', locals(),
                              context_instance=RequestContext(request))

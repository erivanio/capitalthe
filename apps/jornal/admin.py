# -*- econding: utf-8 -*-
from django.contrib import admin
from apps.jornal.models import Jornal

class JornalAdmin(admin.ModelAdmin):
	list_display = ('manchete','dt_edicao')
	prepopulated_fields = {"url" : ("manchete",)} 


admin.site.register(Jornal, JornalAdmin)

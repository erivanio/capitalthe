# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Jornal'
        db.create_table(u'jornal_jornal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('manchete', self.gf('django.db.models.fields.CharField')(max_length=160, null=True, blank=True)),
            ('url', self.gf('django.db.models.fields.SlugField')(max_length=160)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('dt_edicao', self.gf('django.db.models.fields.DateField')()),
            ('codigo', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'jornal', ['Jornal'])


    def backwards(self, orm):
        # Deleting model 'Jornal'
        db.delete_table(u'jornal_jornal')


    models = {
        u'jornal.jornal': {
            'Meta': {'ordering': "('-dt_edicao',)", 'object_name': 'Jornal'},
            'codigo': ('django.db.models.fields.TextField', [], {}),
            'dt_edicao': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manchete': ('django.db.models.fields.CharField', [], {'max_length': '160', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'url': ('django.db.models.fields.SlugField', [], {'max_length': '160'})
        }
    }

    complete_apps = ['jornal']
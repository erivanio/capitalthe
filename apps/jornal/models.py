# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from datetime import datetime
# SIGNALS
from django.db.models import signals
# SLUGIFY
from django.template.defaultfilters import slugify


class Jornal(models.Model):

    manchete = models.CharField('Título da manchete:',max_length=160, blank=True, null = True)
    url = models.SlugField(max_length=160, help_text="Link da edição. Este campo é preenchido automaticamente!")

    # capa_destaque = AutoImageField(upload_to='uploads/jornal/capa/%Y/%m/', max_width=300, max_height=300, 
    # 	help_text="Dimensão 300x300 pixels(fotos fora desta dimensão serão cortadas para este formato)")
    status = models.BooleanField(default=True)
    dt_edicao = models.DateField(verbose_name='Data da Edição', auto_now_add=False)
    codigo = models.TextField('Insira o código EMBED do Issuu')

    class Meta:
        ordering = ('-dt_edicao',)
        verbose_name = u'Jornal'
        verbose_name_plural = u'Jornais'

    def __unicode__(self):
        return self.manchete

    def get_absolute_url(self):
        return ('/jornal/%s/' % (self.url))

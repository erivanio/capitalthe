from annoying.decorators import render_to
from apps.cinema.models import Filme, Slide
from apps.materias.models import Materias, Secao
from django.http import HttpResponse
import json
import urllib2

from datetime import datetime
import datetime, time
from datetime import timedelta
from apps.publicidade.models import Publicidade

agora = (datetime.datetime.now() + timedelta(hours=1)).strftime("%Y-%m-%d %H:00:00")

@render_to('capitalteresina/cultura/cinema.html')
def cinema(request):
    filmes = Filme.objects.filter(status=True).order_by('-cadastrado_em')
    slides = Slide.objects.filter(status=True).order_by('-cadastrado_em')
    ultimas = Materias.objects.filter(status=True).exclude(publicado_em__gte=agora)[:5]
    secoes = Secao.objects.filter(status=True)
    try:
        publicidades_3 = Publicidade.objects.filter(posicao = 4,data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades_3 = []
    return {'filmes': filmes, 'slides': slides, 'ultimas': ultimas, 'publicidades_3': publicidades_3, 'secoes': secoes}

@render_to('capitalteresina/cultura/filme.html')
def filme(request, slug):
    f = Filme.objects.get(slug=slug)
    video = f.trailer.split('=')[-1]
    ultimas = Materias.objects.filter(status = True).exclude(publicado_em__gte=agora)[:5]
    secoes = Secao.objects.filter(status=True)
    try:
        publicidades_3 = Publicidade.objects.filter(posicao = 4,data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades_3 = []
    return {'filme': f, 'video': video, 'ultimas': ultimas, 'publicidades_3': publicidades_3, 'secoes': secoes}


# -*- coding: utf-8 -*-
from django.db import models
from datetime import datetime

SECAO_SALA_CHOICES = (
    ('Sala 01', 'Sala 01'),
    ('Sala 02', 'Sala 02'),
    ('Sala 03', 'Sala 03'),
    ('Sala 04', 'Sala 04'),
    ('Sala 05', 'Sala 05'),
)

HORARIO_DIA_CHOICES = (
    ('segunda', 'Segunda'),
    ('terca', 'Terça'),
    ('quarta', 'Quarta'),
    ('quinta', 'Quinta'),
    ('sexta', 'Sexta'),
    ('sabado', 'Sabado'),
    ('domingo', 'Domingo'),
)

CINEMA_CHOICES = (
    ('0', 'Cinema Teresina'),
    ('1', 'Cinema Riverside'),
)

class Filme(models.Model):
    class Meta:
        ordering = ('cadastrado_em',)

    titulo = models.CharField(max_length=100, help_text='Titulo do filme, no maximo 100 caracteres.')
    genero = models.CharField(max_length=30, help_text='Genero do filme: Ação, Drama, Suspense, Horror... , no maximo 30 caracteres.')
    censura = models.CharField(max_length=30, help_text='Censura do filme: Livre, +12, +14, +16, +18..., no maximo 30 caracteres.')
    duracao = models.CharField(max_length=6, help_text='Duração do filme: 01:43h, 02:03h..., no maximo 6 caracteres.')
    foto = models.ImageField(upload_to='uploads/cinema/%Y/%m/')
    trailer = models.CharField(max_length=200, help_text='Endereço do Youtube')
    slug = models.SlugField(max_length=200, blank=True, )
    sinopse = models.TextField()
    direcao = models.CharField(max_length=50, help_text='Diretor do filme')
    elenco = models.CharField(max_length=300, help_text='Elenco do filme')
    cadastrado_em = models.DateTimeField(verbose_name='Data do Cadastro', blank=True, default=datetime.now, help_text="Data em que este filme foi cadastrado")
    status = models.BooleanField(verbose_name="Status do filme", default=True, help_text="Se está em cartaz ou não.")

    def __unicode__(self):
        return self.titulo

    def get_sessao(self):
        return self.sessao_set.all()[0]

    def get_sessoes(self):
        return self.sessao_set.all()

    def get_trailer(self):
        return "http://www.youtube.com/embed/" + self.trailer.split('=')[-1] + "?autoplay=1"

class Sessao(models.Model):
    class Meta:
        ordering = ('publicado_em',)
        verbose_name = u'Sessão'
        verbose_name_plural = u'Sessões'

    filme = models.ForeignKey(Filme)
    sala = models.CharField(max_length=10, choices=SECAO_SALA_CHOICES, help_text="Sala desta sessão")
    semana = models.CharField(max_length=15, help_text='Semana da sessão: Estréia, 2ª, 3ª...')
    tresd = models.BooleanField(default=False, verbose_name='Sessao 3d?', help_text='Informa se o filme é um filme em 3d')
    legendado = models.BooleanField(default=True, help_text='Informa se o filme é legendado, se não for o filme será considerado dublado.')

    status = models.BooleanField(verbose_name='Sessao Ativa?', default=True)
    cadastrado_em = models.DateTimeField(verbose_name='Data do Cadastro', blank=True, default=datetime.now)
    publicado_em = models.DateTimeField(verbose_name='Data de Publicacao', default=datetime.now)
    cinema = models.CharField("Cinema", choices=CINEMA_CHOICES, max_length=2)

    def __unicode__(self):
        return self.sala

    def get_horario(self):
        return self.horario_set.all()

    def get_cinema(self):
        return CINEMA_CHOICES[int(self.cinema)][1]

class Horario(models.Model):
    class Meta:
        ordering = ('exibicao',)

    sessao = models.ForeignKey(Sessao)
    exibicao = models.CharField(max_length=30, help_text="Horario de exibição desta sessão. Deve ser algo no formato XX:XXh")

    # filme_do_dia = models.BooleanField(verbose_name='Filme do Dia?', default=False, help_text="Informa se esta sessão é uma sessão do filme do dia")
    # dia = models.CharField(max_length=20, choices=HORARIO_DIA_CHOICES, blank=True, help_text="Dia da semana em que o filme do dia será exibido.")

    def __unicode__(self):
        return self.exibicao

class Slide(models.Model):
    foto = models.ImageField(upload_to='uploads/slides/')
    filme = models.ForeignKey('Filme')
    status = models.BooleanField(default=True, help_text="Status do filme")
    cadastrado_em = models.DateTimeField(verbose_name='Data do Cadastro', blank=True, default=datetime.now, help_text="Data em que este slide foi cadastrado")

    def __unicode__(self):
        return self.filme.titulo
# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Filme'
        db.create_table(u'cinema_filme', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('genero', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('censura', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('duracao', self.gf('django.db.models.fields.CharField')(max_length=6)),
            ('foto', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('trailer', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=200, blank=True)),
            ('sinopse', self.gf('django.db.models.fields.TextField')()),
            ('direcao', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('elenco', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('cadastrado_em', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'cinema', ['Filme'])

        # Adding model 'Sessao'
        db.create_table(u'cinema_sessao', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('filme', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cinema.Filme'])),
            ('sala', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('semana', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('tresd', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('legendado', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('cadastrado_em', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('publicado_em', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('cinema', self.gf('django.db.models.fields.CharField')(max_length=2)),
        ))
        db.send_create_signal(u'cinema', ['Sessao'])

        # Adding model 'Horario'
        db.create_table(u'cinema_horario', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sessao', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cinema.Sessao'])),
            ('exibicao', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'cinema', ['Horario'])

        # Adding model 'Slide'
        db.create_table(u'cinema_slide', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('foto', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('filme', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cinema.Filme'])),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('cadastrado_em', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
        ))
        db.send_create_signal(u'cinema', ['Slide'])


    def backwards(self, orm):
        # Deleting model 'Filme'
        db.delete_table(u'cinema_filme')

        # Deleting model 'Sessao'
        db.delete_table(u'cinema_sessao')

        # Deleting model 'Horario'
        db.delete_table(u'cinema_horario')

        # Deleting model 'Slide'
        db.delete_table(u'cinema_slide')


    models = {
        u'cinema.filme': {
            'Meta': {'ordering': "('cadastrado_em',)", 'object_name': 'Filme'},
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'censura': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'direcao': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'duracao': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'elenco': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'genero': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sinopse': ('django.db.models.fields.TextField', [], {}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '200', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'trailer': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'cinema.horario': {
            'Meta': {'ordering': "('exibicao',)", 'object_name': 'Horario'},
            'exibicao': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sessao': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cinema.Sessao']"})
        },
        u'cinema.sessao': {
            'Meta': {'ordering': "('publicado_em',)", 'object_name': 'Sessao'},
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'cinema': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'filme': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cinema.Filme']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legendado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'sala': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'semana': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'tresd': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'cinema.slide': {
            'Meta': {'object_name': 'Slide'},
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'filme': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cinema.Filme']"}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        }
    }

    complete_apps = ['cinema']
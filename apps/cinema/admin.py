# -*- coding: utf-8 -*-
from django.contrib import admin
from datetime import datetime
from .models import Sessao, Filme, Horario, Slide

class HorarioInline(admin.TabularInline):
    model = Horario
    extra = 5

class SessaoAdmin(admin.ModelAdmin):
    list_per_page = 50
    inlines = [HorarioInline]
    fieldsets = [
                 (None, {'fields':['filme', 'cinema', 'sala', 'semana', 'status']}),
                 ('Caracteristicas', {'fields':[('tresd', 'legendado')]}),
                 ('Mais Informações', {'fields':[('publicado_em', 'cadastrado_em')], 'classes': ['collapse closed']}),
    ]
    list_display = ('filme', 'sala', 'semana', 'publicado_em', 'tresd', 'legendado', 'status', 'foto_thumbnail')
    list_filter = ['publicado_em', 'tresd', 'legendado', 'status']
    search_fields = ['filme', 'sala', 'semana']
    date_hierarchy = 'publicado_em'
    
    def save_model(self, request, obj, form, change):
        if not change:
            obj.cadastrado_em = datetime.today()
        obj.save()

    def foto_thumbnail(self, obj):
        try:
            return u'<img height="50px" src="%s" />' % (obj.foto)
        except:
            return "Sem Imagem" 
    foto_thumbnail.short_description = 'Foto'
    foto_thumbnail.allow_tags = True

class FilmeAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('titulo',)}

admin.site.register(Sessao, SessaoAdmin)
admin.site.register(Filme, FilmeAdmin)
admin.site.register(Slide)
# -*- coding: utf-8 -*-
from apps.multimidia.models import Album

from django.template import Library
from django import template

register = Library()

@register.simple_tag(takes_context=True)
def get_album(context, id):
	context['album'] = Album.objects.get(pk = id)
	return ''

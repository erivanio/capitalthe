# -*- coding: utf-8 -*-
from django.template import Library
from portalpmt.album.models import Album

register = Library()

@register.inclusion_tag('portalpmt/componentes/albuns_destaque.html')
def albuns_destaque():
	albuns = [1,2,3,4,5,6,7]
	destaques = Album.objects.filter(status=True)[:6]
	#destaque = destaques4[0]
	#destaques = destaques4[1:]
	return locals()
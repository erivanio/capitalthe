from django.contrib.admin.views.main import ALL_VAR, PAGE_VAR
from django.utils.html import escape
from django.template import Library

register = Library()
@register.simple_tag
def static(url):
	"""
	If set, returns the string contained in the setting ADMIN_MEDIA_PREFIX, otherwise returns STATIC_URL + 'admin/'.
	"""
	newurl = '/static/'+url
	return newurl
# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'WebSite'
        db.create_table('multimidia_website', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('site', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('path', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('path_titulo', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('path_subtitulo', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('path_texto', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('path_categoria', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('path_data', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('formato_data', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('start_page', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('domains', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal('multimidia', ['WebSite'])


        # Changing field 'Foto.legenda'
        db.alter_column('multimidia_foto', 'legenda', self.gf('django.db.models.fields.CharField')(max_length=200))

    def backwards(self, orm):
        # Deleting model 'WebSite'
        db.delete_table('multimidia_website')


        # Changing field 'Foto.legenda'
        db.alter_column('multimidia_foto', 'legenda', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        'multimidia.album': {
            'Meta': {'ordering': "('-cadastrado_em',)", 'object_name': 'Album'},
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'creditos': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'foto_big': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'foto_destaque': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'foto_medium': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'foto_small': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'visualizacoes': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'multimidia.download': {
            'Meta': {'ordering': "('-publicado_em',)", 'object_name': 'Download'},
            'arquivo': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'visualizacoes': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'multimidia.foto': {
            'Meta': {'ordering': "('-cadastrado_em',)", 'object_name': 'Foto'},
            'album': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['multimidia.Album']"}),
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'foto_thumb': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legenda': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'multimidia.video': {
            'Meta': {'ordering': "('-publicado_em',)", 'object_name': 'Video'},
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'video': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'video_id': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'visualizacoes': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'multimidia.website': {
            'Meta': {'object_name': 'WebSite'},
            'domains': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'formato_data': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'path_categoria': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'path_data': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'path_subtitulo': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'path_texto': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'path_titulo': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'start_page': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['multimidia']
# -*- coding: utf-8 -*-
from django.db import models
from datetime import datetime
from django.db.models import signals
from django.template.defaultfilters import slugify
from image_cropping import ImageRatioField
from easy_thumbnails.files import get_thumbnailer
from tagging.utils import parse_tag_input


class Album(models.Model):
    class Meta:
        ordering = ('-cadastrado_em',)
        verbose_name = u'Album'
        verbose_name_plural = u'Albuns'

    titulo = models.CharField(max_length=100, help_text="Titulo do album com até 100 caracteres.")
    foto_destaque = models.ImageField(upload_to='uploads/album/%Y/%m/', help_text="Foto destaque do Album. Dimensões 120x80")
    foto_medium = ImageRatioField('foto_destaque', '190x135')
    foto_big = ImageRatioField('foto_destaque', '311x200')
    foto_small = ImageRatioField('foto_destaque', '142x100')
    slug = models.SlugField(max_length=100, blank=True, unique=True)
    creditos = models.CharField(max_length = 150, blank = True, null = True)

    visualizacoes = models.IntegerField(default=0)
    publicado_em = models.DateTimeField(verbose_name='Data de Publicacao', default=datetime.now)
    cadastrado_em = models.DateTimeField(verbose_name='Data do Cadastro', default=datetime.now, blank=True)
    status = models.BooleanField(verbose_name='Ativo? ', default=True)

    def __unicode__(self):
        return self.titulo

    def get_absolute_url(self):
        return ('/albuns/%s-%i.html' % (self.slug, self.id))

    def get_tag_list(self):
        return parse_tag_input(self.tags)

    def get_medium(self):
        return get_thumbnailer(self.foto_destaque).get_thumbnail({
            'size': (201, 150),
            'box': self.foto_medium,
            'crop': True,
            'detail': True,
            }).url

    def get_big(self):
        return get_thumbnailer(self.foto_destaque).get_thumbnail({
            'size': (294, 150),
            'box': self.foto_big,
            'crop': True,
            'detail': True,
            }).url

    def get_big_interna(self):
        return get_thumbnailer(self.foto_destaque).get_thumbnail({
            'size': (311, 200),
            'box': self.foto_big,
            'crop': True,
            'detail': True,
            }).url

    def get_small(self):
        return get_thumbnailer(self.foto_destaque).get_thumbnail({
            'size': (155, 150),
            'box': self.foto_small,
            'crop': True,
            'detail': True,
            }).url
    def imagemAdmin(self):
        if self.foto_destaque:
            try:
                im = get_thumbnailer(self.foto_destaque).get_thumbnail({
                    'size': (311, 200),
                    'box': self.foto_big
                })
                return '<img src="{0}" />'.format(im.url)
            except:
                return ''
        return ''
    imagemAdmin.is_safe = True
    imagemAdmin.allow_tags = True
    imagemAdmin.short_description = u'Imagem'

class Foto(models.Model):
    class Meta:
        ordering = ('-cadastrado_em',)

    foto = models.ImageField(upload_to='uploads/album/fotos/%Y/%m/')#, size=(530, 397)
    legenda = models.CharField(max_length=200, blank=True)
    foto_thumb = ImageRatioField('foto','142x100')

    album = models.ForeignKey(Album)

    cadastrado_em = models.DateTimeField(verbose_name='Data do Cadastro', default=datetime.now, blank=True, null = True)
    status = models.BooleanField(verbose_name='Ativo? ', default=True)

    def imagem(self):
        if self.foto:
            try:
                im = get_thumbnailer(self.foto).get_thumbnail({
                    'size': (135, 90),
                    'box': self.foto_thumb
                })
                return im.url
            except:
                return ''

        return ''
    def imagemAdmin(self):
        if self.foto:
            try:
                im = get_thumbnailer(self.foto).get_thumbnail({
                    'size': (135, 90),
                    'box': self.foto_thumb
                })
                return '<img src="{0}" />'.format(im.url)
            except:
                return ''
        return ''
    imagemAdmin.is_safe = True
    imagemAdmin.allow_tags = True
    imagemAdmin.short_description = u'Imagem'

    def __unicode__(self):
        return self.legenda

    def get_foto(self):
        return "/media/%s" % self.foto

    def get_thumb(self):
        return get_thumbnailer(self.foto).get_thumbnail({
            'size': (142, 100),
            'box': self.foto_thumb
        }).url

    def get_image(self):
        return "/media/%s" % self.foto

def album_pre_save(signal, instance, sender, **kwargs):
    """Este signal gera um slug automaticamente. Ele verifica se ja existe um
    artigo com o mesmo slug e acrescenta um numero ao final para evitar
    duplicidade"""
    if not instance.slug:
        slug = slugify(instance.titulo)
        novo_slug = slug
        contador = 0

        while Album.objects.filter(slug=novo_slug).exclude(id=instance.id).count() > 0:
            contador += 1
            novo_slug = '%s-%d'%(slug, contador)

        instance.slug = novo_slug
signals.pre_save.connect(album_pre_save, sender=Album)

class Video(models.Model):
    class Meta:
        ordering = ('-publicado_em',)

    titulo = models.CharField(max_length=100, help_text="Tamanho maximo de 100 caracteres.")
    visualizacoes = models.IntegerField(default=0)
    slug = models.SlugField(max_length=100, blank=True, unique=True)

    foto = models.ImageField(upload_to='uploads/video/%Y/%m/', null = True, blank = True)
    video = models.CharField(max_length=200, help_text="Url do Youtube")
    video_id = models.CharField(max_length=30)

    publicado_em = models.DateTimeField(verbose_name='Data de Publicacao', default=datetime.now, blank=True, help_text="Data em que este video deve ser publicado.")
    cadastrado_em = models.DateTimeField(verbose_name='Data do Cadastro', default=datetime.now, blank=True)
    status = models.BooleanField(default=True)

    def __unicode__(self):
        return self.titulo

    def get_absolute_url(self):
        return ('/videos/%s-%i.html' % (self.slug, self.id))

    def get_tag_list(self):
        return parse_tag_input(self.tags)

def video_pre_save(signal, instance, sender, **kwargs):
    """Este signal gera um slug automaticamente. Ele verifica se ja existe um
    artigo com o mesmo slug e acrescenta um numero ao final para evitar
    duplicidade"""
    novo_slug = None
    if not instance.slug:
        slug = slugify(instance.titulo)
        novo_slug = slug
        contador = 0

        while Video.objects.filter(slug=novo_slug).exclude(id=instance.id).count() > 0:
            contador += 1
            novo_slug = '%s-%d'%(slug, contador)

        instance.slug = novo_slug

signals.pre_save.connect(video_pre_save, sender=Video)

class Download(models.Model):
    class Meta:
        ordering = ('-publicado_em',)

    titulo = models.CharField(max_length=100, help_text="Tamanho maximo de 100 caracteres.")
    visualizacoes = models.IntegerField(default=0)
    slug = models.SlugField(max_length=100, blank=True, unique=True)

    arquivo = models.FileField(upload_to='uploads/downloads/%Y/%m/', help_text="Arquivo a ser baixado")

    publicado_em = models.DateTimeField(verbose_name='Data de Publicacao', default=datetime.now, blank=True, help_text="Data em que este video deve ser publicado.")
    cadastrado_em = models.DateTimeField(verbose_name='Data do Cadastro', default=datetime.now, blank=True)
    status = models.BooleanField(default=True)

    def __unicode__(self):
        return self.titulo

    def get_absolute_url(self):
        return ('/videos/%s-%i.html' % (self.slug, self.id))

    def get_tag_list(self):
        return parse_tag_input(self.tags)

def download_pre_save(signal, instance, sender, **kwargs):
    """Este signal gera um slug automaticamente. Ele verifica se ja existe um
    artigo com o mesmo slug e acrescenta um numero ao final para evitar
    duplicidade"""
    novo_slug = None
    if not instance.slug:
        slug = slugify(instance.titulo)
        novo_slug = slug
        contador = 0

        while Download.objects.filter(slug=novo_slug).exclude(id=instance.id).count() > 0:
            contador += 1
            novo_slug = '%s-%d'%(slug, contador)

        instance.slug = novo_slug

signals.pre_save.connect(download_pre_save, sender=Download)

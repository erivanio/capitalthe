# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.template import Context, RequestContext
from django.shortcuts import render_to_response
from apps.materias.models import Materias, Secao
from apps.multimidia.models import Album, Foto, Video
from django.shortcuts import get_object_or_404
from django.db.models import Q
from datetime import datetime
from django.views.generic import CreateView, DeleteView
from apps.multimidia.forms import FotoForm
from django.core.urlresolvers import reverse
from django.utils import simplejson
from django.http import HttpResponse, HttpResponseRedirect
from apps.publicidade.models import Publicidade


def album(request, slug, id):
    album = get_object_or_404(Album, slug = slug,id=id, status = True)
    albuns = Album.objects.filter(status=True).exclude(pk= album.pk).exclude(publicado_em__gte=datetime.now()).order_by('-publicado_em')
    try:
        titulo = album.titulo + ' - '
    except:
        titulo = ''

    return render_to_response('capitalteresina/album/album.html', locals(),
                              context_instance=RequestContext(request))

def central_albuns(request):
    albuns = Album.objects.filter(status=True).exclude(publicado_em__gte=datetime.now()).order_by('-publicado_em')
    ultimas = Materias.objects.filter(status = True).exclude(publicado_em__gte=datetime.today())[:5]
    secoes = Secao.objects.filter(status = True)
    try:
        publicidades_3 = Publicidade.objects.filter(posicao = 4,data_entrada__lte=datetime.now(),data_saida__gte=datetime.now()).order_by('?')[0]
    except:
        publicidades_3 = []
    try:
        titulo = 'Álbuns'
    except:
        titulo = ''
    paginator = Paginator(albuns, 6)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        albuns = paginator.page(page)
    except (EmptyPage, InvalidPage):
        albuns = paginator.page(paginator.num_pages)

    return render_to_response('capitalteresina/album/central.html', locals(),
                              context_instance=RequestContext(request))

def central_videos(request):
    videos = Video.objects.filter(status=True).exclude(publicado_em__gte=datetime.now()).order_by('-publicado_em')
    first_video = videos[0]

    try:
        titulo = 'Vídeos'
    except:
        titulo = ''

    return render_to_response('videos/central.html', locals(),
                              context_instance=RequestContext(request))

def busca(request):

    try:
        queryb = request.GET['q']
        if queryb:
            qset = (
                Q(titulo__icontains=queryb) |
                Q(chapeu__icontains=queryb)
            )
    except:
        try:
            queryb = request.GET['palavra']
            if queryb:
                qset = (
                    Q(titulo__icontains=queryb) |
                    Q(chapeu__icontains=queryb)
                )
        except:
            queryb = None
    if queryb:
        ensaios = Album.objects.filter(qset)

    return render_to_response('capitalteresina/album/listagem.html', locals(),
                              context_instance=RequestContext(request))

def listar_videos(request):
    videos = Video.objects.filter(status=True).order_by('publicado_em')
    titulo = 'Últimos vídeos'

    return render_to_response('capitalteresina/videos/listagem_videos.html', locals(),
                              context_instance=RequestContext(request))

def ver_video(request, slug, id):
    titulo = 'Ver vídeo'
    try:
        video = Video.objects.get(slug=slug, id=id)
    except Video.DoesNotExist:
        raise Http404

    return render_to_response('capitalteresina/videos/ver_video.html', locals(),
                              context_instance=RequestContext(request))

def response_mimetype(request):
    if "application/json" in request.META['HTTP_ACCEPT']:
        return "application/json"
    else:
        return "text/plain"

class PictureCreateView(CreateView):
    model = Foto
    form_class = FotoForm

    def dispatch(self, request, *args, **kwargs):
        return super(PictureCreateView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self, **kwargs):
        kwargs = super(PictureCreateView, self).get_form_kwargs(**kwargs)
        if 'album_id' in self.kwargs:
            album = Album.objects.get(pk=self.kwargs['album_id'])
            instance = Foto(album = album,cadastrado_em = datetime.today(), status = True)
            kwargs.update({'instance': instance})
        return kwargs

    def form_valid(self, form):
        self.object = form.save()
        f = self.request.FILES.get('foto')
        data = [{'name': f.name, 'url': self.object.get_image(), 'thumbnail_url': self.object.get_image(), 'delete_url': reverse('upload-delete', args=[self.object.id]), 'delete_type': "DELETE"}]
        response = JSONResponse(data, {}, response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class PictureDeleteView(DeleteView):
    model = Foto

    def dispatch(self, request, *args, **kwargs):
        return super(PictureDeleteView, self).dispatch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """
        This does not actually delete the file, only the database record.  But
        that is easy to implement.
        """
        self.object = self.get_object()
        self.object.delete()
        if request.is_ajax():
            response = JSONResponse(True, {}, response_mimetype(self.request))
            response['Content-Disposition'] = 'inline; filename=files.json'
            return response
        else:
            return HttpResponseRedirect('/upload/new')

class JSONResponse(HttpResponse):
    """JSON response class."""
    def __init__(self,obj='',json_opts={},mimetype="application/json",*args,**kwargs):
        content = simplejson.dumps(obj,**json_opts)
        super(JSONResponse,self).__init__(content,mimetype,*args,**kwargs)
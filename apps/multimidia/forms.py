from django import forms
from apps.multimidia.models import Foto, Album

class FotoForm(forms.ModelForm):
	album = forms.ModelChoiceField(queryset=Album.objects.all(), widget=forms.HiddenInput())

	class Meta:
		model = Foto
		exclude = ('foto_thumb','legenda')
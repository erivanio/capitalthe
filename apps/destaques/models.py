# -*- encoding: utf-8 -*-
from django.db import models
from apps.materias.models import Materias
from PIL import Image
from datetime import datetime
import sys, time
from image_cropping import ImageRatioField
from easy_thumbnails.files import get_thumbnailer
from django.db.models import signals


DESTAQUES_POSICAO_CHOICES = (
    ('1', 'Principal 567x325'),
    ('2', 'Destaque Foto Cima 397x150'),
    ('3', 'Destaque Foto Baixo 183x150'),
    ('4', 'Destaque Foto Principal 1 311x150'),
    ('5', 'Destaque Foto Principal 2 311x150'),
    ('6', 'Destaque Foto Principal 3 311x150'),
    ('7', 'Destaque Texto Baixo 1'),
    ('8', 'Destaque Texto Baixo 2'),
    ('9', 'Destaque Texto Baixo 3'),
    ('10', 'Destaque Texto Baixo 4'),
    ('11', 'Destaque Especiais 183x150'),
    ('12', 'Destaque Especiais 567x325'),
    ('13', 'Destaque Texto Centro 1'),
    ('14', 'Destaque Texto Centro 2'),
    ('15', 'Destaque Texto Centro 3'),
)

class Destaque(models.Model):

    filepath = 'uploads/destaques/%Y/%m/'

    posicao = models.CharField(verbose_name='Posicao de Destaque',
                               max_length=2, choices=DESTAQUES_POSICAO_CHOICES)
    materia = models.ForeignKey(Materias, blank=True, null=True)
    link = models.CharField(max_length=100, help_text="Link para a matéria", blank=True, null=True)
    status = models.BooleanField(default = True)
    titulo = models.CharField(max_length=100, help_text="Para Manchetes, Destaques com Foto e Destaques sem Foto")
    subtitulo = models.CharField(max_length=100, blank=True, help_text="Para Manchetes")
    chapeu = models.CharField(max_length=255, blank=True, help_text="Para Destaques com Foto e Destaques sem Foto")
    foto = models.ImageField(blank=True, null=True, upload_to=filepath)
    foto_medium = ImageRatioField('foto', '567x325')
    foto_thumb = ImageRatioField('foto', '397x150')
    foto_small = ImageRatioField('foto', '183x150')
    foto_intermediate = ImageRatioField('foto', '311x150')
    publicado_em = models.DateTimeField(default = datetime.now)

    def __unicode__(self):
        return self.titulo

    def get_absolute_url(self):
        if self.materia:
            return self.materia.get_absolute_url()
        return self.link

    def get_destaque_principal(self):
        return get_thumbnailer(self.foto).get_thumbnail({
            'size': (567, 325),
            'box': self.foto_medium,
            'crop': True,
            'detail': True,
            }).url

    def get_destaque_secundario(self):
        return get_thumbnailer(self.foto).get_thumbnail({
            'size': (397, 150),
            'box': self.foto_thumb,
            'crop': True,
            'detail': True,
            }).url

    def get_destaque_small(self):
        return get_thumbnailer(self.foto).get_thumbnail({
            'size': (183, 150),
            'box': self.foto_small,
            'crop': True,
            'detail': True,
            }).url
    def get_destaque_intermediate(self):
        return get_thumbnailer(self.foto).get_thumbnail({
            'size': (311, 150),
            'box': self.foto_intermediate,
            'crop': True,
            'detail': True,
            }).url
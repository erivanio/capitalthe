from django import forms
from apps.destaques.models import Destaque
from apps.materias.models import Materias


class DestaqueForm(forms.ModelForm):
    titulo = forms.CharField(max_length = 60)
    # foto = forms.ImageField('uploads/destaques/%Y/%m/')
    # titulo.max_length = 50

    class Meta:
        model = Destaque
        exclude = ('created_at','subtitulo')

    def clean_titulo(self):
        sem_imagens =[7,8,9,10,13,14,15]
        if int(self.cleaned_data['posicao']) in sem_imagens and len(self.cleaned_data['titulo']) > 85:
            raise forms.ValidationError("Destaques desse tipo so podem ter titulo com no maximo 85 caracteres")
        return self.cleaned_data['titulo']

    def clean_foto(self):
        destaque_com_imagens =[1,2,3,4,5,6,11,12]
        if int(self.cleaned_data['posicao']) in destaque_com_imagens and not self.cleaned_data['foto']:
            raise forms.ValidationError("Campo imagem obrigatorio nesse tipo de destaque.")
        return self.cleaned_data['foto']

    def __init__(self, *args, **kwargs):
        super(DestaqueForm, self).__init__(*args, **kwargs)
        try:
            materia_id = kwargs['initial']['materia_id']
            m = Materias.objects.get(id=materia_id)
            self.initial['materia'] =  materia_id
            self.initial['titulo'] = m.titulo
        except Exception,e:
            pass

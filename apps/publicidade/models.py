# -*- coding: utf-8 -*-
from datetime import datetime
from django.db import models

POSICAO_CHOICES = (('1', '728x90'), ('2', '225x90'), ('3', '225x262'), ('4', '300x250'))

class Publicidade(models.Model):
    class Meta:
        verbose_name_plural = "Publicidades"
    nome=models.CharField(max_length=100)
    arquivo=models.FileField(upload_to='Publicidade')
    data_entrada=models.DateTimeField("Data de entrada",default=datetime.now)
    data_saida=models.DateTimeField("Data de saída")
    posicao=models.CharField('Tipo da publicidade', max_length=1, choices = POSICAO_CHOICES)
    url=models.URLField("Site da publicidade", blank=True, null=True)

    def __unicode__(self):
        return self.nome

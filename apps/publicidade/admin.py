# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.publicidade.models import Publicidade

class PublicidadesAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Publicidade", {'fields': ['posicao', 'nome', 'url']}), ("Upload", {'fields': ['arquivo']}), ("Período", {'fields': ['data_entrada', 'data_saida']})
    ]
    list_display = ('nome', 'posicao')
    list_filter = ['posicao', 'nome']
    search_fields = ['nome', 'posicao']
    date_hierarchy = 'data_entrada'

admin.site.register(Publicidade, PublicidadesAdmin)
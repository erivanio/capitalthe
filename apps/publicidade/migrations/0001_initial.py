# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Publicidade'
        db.create_table(u'publicidade_publicidade', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('arquivo', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('data_entrada', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('data_saida', self.gf('django.db.models.fields.DateTimeField')()),
            ('posicao', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'publicidade', ['Publicidade'])


    def backwards(self, orm):
        # Deleting model 'Publicidade'
        db.delete_table(u'publicidade_publicidade')


    models = {
        u'publicidade.publicidade': {
            'Meta': {'object_name': 'Publicidade'},
            'arquivo': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'data_entrada': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'data_saida': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'posicao': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['publicidade']
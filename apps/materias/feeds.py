# -*- coding: utf-8 -*-
from django.contrib.syndication.views import Feed
from apps.materias.models import Materias, Nota

class UltimasNoticias(Feed):
    title = "Últimas Notícias do Capital Teresina"
    link = '/'

    def items(self):
        return Materias.objects.all().order_by('-publicado_em')

    def item_link(self, materias):
        return '/noticias/%s/%s-%i.html' % (materias.secao.url,materias.slug, materias.id)

class UltimasNotas(Feed):
    title = "Últimas Notas do Capital Teresina"
    link = '/'

    def items(self):
        return Nota.objects.all().order_by('-publicado_em')

    def item_link(self, notas):
        return '/colunas/%s/%s-%s.html' % (notas.secao.url, notas.slug, notas.id)
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.contrib.sitemaps import ping_google
from datetime import datetime
from django.contrib.contenttypes.models import ContentType
from django.db.models import signals
from django.template.defaultfilters import slugify
from apps.multimidia.models import Album, Video
from redactor.fields import RedactorField
from easy_thumbnails.fields import ThumbnailerImageField
from django.conf import settings
from image_cropping import ImageRatioField
from easy_thumbnails.files import get_thumbnailer
from tagging.fields import TagField
from tagging.utils import parse_tag_input
import lxml.html as lh
from lxml.html import builder


class Secao(models.Model):
    nome = models.CharField('Nome da Secao',max_length=35)
    url = models.SlugField('Url da Secao',max_length=35)
    descricao = models.CharField('Descricao',max_length = 150)
    dt_cadastro = models.DateTimeField(verbose_name='Data de Cadastro', auto_now_add=True)
    dt_ultAtualizacao = models.DateTimeField(verbose_name='Data Ultima Atualizacao',blank=True,default=datetime.now)
    status = models.BooleanField(verbose_name="Status", default=True)
    rodape = models.BooleanField(verbose_name="Rodape", default=False)
    secao_origem = models.ForeignKey('Secao', blank=True, null=True)

    class Meta:
        ordering = ('nome',)
        verbose_name = 'Secao'
        verbose_name_plural = 'Secoes'

    def get_absolute_url(self):
        return ('/noticias/%s/' % (self.url))

    def __unicode__(self):
        return self.nome


class Materias(models.Model):
    class Meta:
        ordering = ('-publicado_em',)
        verbose_name = u'Materia'
        verbose_name_plural = u'Materias'
        get_latest_by = 'publicado_em'

    titulo = models.CharField(max_length=90, help_text="Titulo da Noticia")
    subtitulo = models.CharField(max_length=100, help_text="Para Manchetes")
    texto = RedactorField(verbose_name="Texto")
    slug = models.SlugField(max_length=200, blank=True, unique=True)
    url = models.SlugField(max_length=200, blank=True, unique=True)

    album = models.ForeignKey(Album, blank=True, null=True,
                              help_text="Album relacionado a esta notícia. Caso esta matéria queira exibir mais de uma foto relacionada dentro da notícia.")

    # foto = models.ImageField(upload_to='uploads/materia/%Y/%m/')
    foto = ThumbnailerImageField(upload_to='materia/%Y/%m/', blank=True, help_text ="Foto destaque na materia. Dimensões 300x250")
    foto_materia = ImageRatioField('foto', '300x250')
    legenda = models.CharField(max_length=50, blank=True,
                               help_text="Se uma foto for inserida, a legenda deve ser preenchida! tamanho maximo de 30 caracteres.")
    credito = models.CharField(max_length=30, blank=True,
                               help_text="Creditos da foto.")

    # foto_destaque = models.ImageField(upload_to='uploads/materia/%Y/%m/', blank = True, null = True)
    foto_destaque = models.ImageField(upload_to='destaques/%Y/%m/', blank=True, help_text="Foto destaque dentro da materia. Dimensões 750x240")
    foto_destaque_materia = ImageRatioField('foto_destaque', '750x240')
    legenda_destaque = models.CharField(max_length=50, blank=True,
                                        help_text="Se uma foto de destaque for inserida, a legenda deve ser preenchida! tamanho maximo de 30 caracteres.")
    credito_destaque = models.CharField(max_length=30, blank=True,
                                        help_text="Creditos da foto de destaque.")

    ativarvideo = models.BooleanField(verbose_name='Utilizar Video', default=False,
                                      help_text="Habilitar o uso de um video relacionado a esta noticia")
    video = models.ForeignKey(Video, blank=True, null=True, verbose_name='Video',
                              help_text="Video relacionado a esta noticia. ")

    publicado_em = models.DateTimeField(verbose_name='Data de Publicacao', default=datetime.now)

    ativarcomentarios = models.BooleanField(verbose_name='Utilizar comentarios', default=True,
                                            help_text="Habilitar o uso de comentarios do Facebook")

    status = models.BooleanField(verbose_name='Noticia Ativa?', default=True,
                                 help_text="Se esta opção estiver desmarcada os usuários do portal não irão mais ver esta notícia")
    #secoes
    secao = models.ForeignKey(Secao, blank=False, null=False)

    visualizacoes = models.IntegerField(default=0)
    criador = models.ForeignKey(User, blank=True, null=True, related_name='criador')
    cadastrado_em = models.DateTimeField(verbose_name='Data do Cadastro', blank=True)
    alterador = models.ForeignKey(User, blank=True, null=True, related_name='alterador')
    alterado_em = models.DateTimeField(verbose_name='Data da ultima alteracao', blank=True)

    fonte = models.CharField(max_length = 200, null = True, blank = True)
    autor =models.CharField(max_length = 200, null = True, blank = True)
    palavras_chaves  = TagField('palavras chaves separadas por vírgula')

    def nomeBusca(self):
        return self.titulo

    def __unicode__(self):
        return self.titulo

    def get_absolute_url(self):
        try:
            return ('/noticias/%s/%s-%i.html' % (self.secao.url,self.slug, self.id))
        except:
            pass

    def get_print_url(self):
        try:
            return ('/noticias/imprimir/%s/%s-%i.html' % (self.secao.url,self.slug, self.id))
        except:
            pass

    def get_foto(self):
        return "/media/%s" % self.foto

    def save(self, force_insert=False, force_update=False):
        super(Materias, self).save(force_insert, force_update)
        try:
            ping_google()
        except Exception:
            pass

    def get_image_materia(self):
        return get_thumbnailer(self.foto).get_thumbnail({
            'size': (300, 250),
            'box': self.foto_materia,
            'crop': True,
            'detail': True,
            }).url

    def get_thumb_materia(self):
        return get_thumbnailer(self.foto).get_thumbnail({
            'size': (75, 75),
            'box': self.foto_materia,
            'crop': True,
            'detail': True,
            }).url

    def get_image_medium_materia(self):
        return get_thumbnailer(self.foto).get_thumbnail({
            'size': (120, 60),
            'box': self.foto_materia,
            'crop': True,
            'detail': True,
            }).url

    def get_image_full_materia(self):
        return get_thumbnailer(self.foto_destaque).get_thumbnail({
            'size': (750, 240),
            'box': self.foto_destaque_materia,
            'crop': True,
            'detail': True,
            }).url

    def set_tags(self, tags):
        Tag.objects.update_tags(self, tags)

    def get_tags(self, tags):
        return Tag.objects.get_for_object(self)

    def get_palavras_chaves(self):
        return parse_tag_input(self.palavras_chaves)

class Frase(models.Model):
    materia = models.ForeignKey(Materias)
    autor = models.CharField(max_length = 50)
    frase = models.CharField(max_length = 200)
    paragrafo = models.IntegerField()

class Coluna(models.Model):
    secao = models.ForeignKey(Secao)
    user = models.OneToOneField(User)
    biografia = models.TextField()
    tipo = models.CharField(max_length = 30, choices = (('Social','Social'),('Opiniao','Opiniao')))
    imagem = models.ImageField(upload_to='uploads/colunas/', help_text = "Imagem menor. Dimensões 70x70")
    imagem_thumb = ImageRatioField('foto', '70x70')
    imagem_media = models.ImageField(upload_to='uploads/colunas/', help_text = "Imagem média. Dimensões 345x100")
    imagem_media_thumb = ImageRatioField('foto', '345x100')
    imagem_header = models.ImageField(upload_to='uploads/colunas/', help_text = "Imagem no Header da Coluna. Dimensões 745x140")
    imagem_header_thumb = ImageRatioField('foto', '750x140')

    def __unicode__(self):
        return self.secao.nome

    def get_absolute_url(self):
        try:
            return ('/colunas/%s/' % (self.secao.url))
        except:
            pass

    def get_image_small(self):
        return get_thumbnailer(self.imagem).get_thumbnail({
            'size': (119, 237),
            'box': self.imagem_thumb,
            'crop': True,
            'detail': True,
            }).url

    def get_image_medium(self):
        return get_thumbnailer(self.imagem_media).get_thumbnail({
            'size': (345, 100),
            'box': self.imagem_media_thumb,
            'crop': True,
            'detail': True,
            }).url

    def get_image_header(self):
        return get_thumbnailer(self.imagem_header).get_thumbnail({
            'size': (750, 140),
            'box': self.imagem_header_thumb,
            'crop': True,
            'detail': True,
            }).url


class Nota(models.Model):
    titulo = models.CharField(max_length=100)
    nota = RedactorField('Nota',blank = True, null = True)
    primeira_foto = models.ImageField(upload_to='uploads/notas/%Y/%m/', blank = True, null = True)
    legenda_primeira_foto = models.CharField(max_length = 200, blank = True, null = True)
    segunda_foto = models.ImageField(upload_to='uploads/notas/%Y/%m/', blank = True, null = True)
    legenda_segunda_foto = models.CharField(max_length = 200, blank = True, null = True)
    secao = models.ForeignKey(Secao)
    album = models.ForeignKey(Album, blank=True, null=True,
                              help_text="Album relacionado a esta nota")
    slug = models.SlugField(max_length=200, unique=True)
    publicado_em = models.DateTimeField(default=datetime.now)
    user = models.ForeignKey(User, blank=True, null=True)
    ordem = models.IntegerField(default=99)

    def __unicode__(self):
        return self.titulo

    def get_absolute_url(self):
        try:
            return ('/colunas/%s/%s-%s.html' % (self.secao.url, self.slug, self.id))
        except:
            pass


class Infografico(models.Model):
    descricao = models.CharField('Legenda', max_length=50)
    creditos = models.CharField('Creditos', max_length=50)
    imagem = models.ImageField(upload_to='uploads/infograficos/%Y/%m', blank = True, null = True)
    materia = models.ForeignKey('Materias')
    publicado_em = models.DateTimeField('Data de Publicacao', default=datetime.now)

    def __unicode__(self):
        return self.materia.titulo


def materias_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        slug = slugify(instance.titulo)
        instance.slug = slugify(instance.titulo)
        novourl = 'noticias/%s/%s'%(instance.secao.url, slug)
        instance.url = novourl


def materias_post_save(signal, instance, sender, **kwargs):
    """Este signal atualiza a data de atualização da seção"""
    secao = instance.secao
    secao.dt_ultAtualizacao = datetime.today()
    secao.save()


def frase_post_save(signal, instance, sender, **kwargs):
    materia = instance.materia
    soup = lh.fragments_fromstring(materia.texto)
    quote = builder.BLOCKQUOTE(builder.CLASS('fl'), builder.P(instance.frase), builder.SPAN(instance.autor))
    soup.insert(int(instance.paragrafo), quote)
    materia.texto = ""
    for s in soup:
        materia.texto += lh.tostring(s, pretty_print=True)
    materia.save()


def frase_post_delete(signal, instance, sender, **kwargs):
    materia = instance.materia
    soup = lh.fragments_fromstring(materia.texto)
    soup.remove(soup[instance.paragrafo])
    materia.texto = ""
    for s in soup:
        materia.texto += lh.tostring(s, pretty_print=True)
    materia.save()


signals.pre_save.connect(materias_pre_save, sender=Materias)
signals.post_save.connect(materias_post_save, sender=Materias)
signals.post_save.connect(frase_post_save, sender=Frase)
signals.post_delete.connect(frase_post_delete, sender=Frase)

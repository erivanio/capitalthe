    # -*- coding: utf-8 -*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from apps.materias.models import Materias, Secao, Coluna, Nota, Infografico
from django.shortcuts import get_object_or_404
from django.db.models import Q
from datetime import datetime
from django.core.paginator import Paginator, InvalidPage, EmptyPage
import datetime, time
from datetime import timedelta
from apps.publicidade.models import Publicidade
agora = (datetime.datetime.now() + timedelta(hours=1)).strftime("%Y-%m-%d %H:00:00")
import json
from django.http import HttpResponse
import lxml.html as lh
from lxml.html import builder


def ver_materia(request, categoria, slug, id):
    try:
        publicidades_3 = Publicidade.objects.filter(posicao = 4,data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades_3 = []
    try:
        publicidades = Publicidade.objects.filter(posicao=1, data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades = []
    noticia = get_object_or_404(Materias, slug = slug, id=id, status = True)
    secoes = Secao.objects.filter(status = True)
    relacionadas = Materias.objects.filter(secao = noticia.secao, status = True).exclude(publicado_em__gte=datetime.datetime.today()).exclude(id=noticia.id)[:4]
    ultimas = Materias.objects.filter(status = True).exclude(publicado_em__gte=datetime.datetime.today())[:5]
    tags = noticia.palavras_chaves.split(',')
    try:
        titulo = noticia.titulo + ' - '
    except:
        titulo = ''

    return render_to_response('capitalteresina/noticias/ver.html', locals(),
                              context_instance=RequestContext(request))

def imprimir_materia(request, categoria, slug, id):
    noticia = get_object_or_404(Materias, slug = slug, id=id, status = True)
    try:
        publicidades = Publicidade.objects.filter(posicao=1, data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades = []
    try:
        titulo = noticia.titulo + ' - '
    except:
        titulo = ''

    return render_to_response('capitalteresina/noticias/imprimir.html', locals(),
                              context_instance=RequestContext(request))

def noticias(request):
    try:
        publicidades_3 = Publicidade.objects.filter(posicao = 4,data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades_3 = []
    noticias = Materias.objects.filter(status=True).exclude(publicado_em__gte=datetime.datetime.today()).order_by('-publicado_em')
    secoes = Secao.objects.filter(status = True)
    ultimas = Materias.objects.filter(status = True).exclude(publicado_em__gte=datetime.datetime.today())[:5]
    paginator = Paginator(noticias, 10)
    titulo = 'Notícias'
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        noticias = paginator.page(page)
    except (EmptyPage, InvalidPage):
        noticias = paginator.page(paginator.num_pages)
    return render_to_response('capitalteresina/noticias/listagem.html', locals(),
                              context_instance=RequestContext(request))

def artigos(request):
    try:
        publicidades_3 = Publicidade.objects.filter(posicao = 4,data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades_3 = []
    noticias = Materias.objects.filter(status=True, secao__url='artigos').exclude(publicado_em__gte=datetime.datetime.today()).order_by('-publicado_em')
    ultimas = Materias.objects.filter(status = True).exclude(publicado_em__gte=datetime.datetime.today())[:5]
    paginator = Paginator(noticias, 10)
    titulo = 'Artigos'
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        noticias = paginator.page(page)
    except (EmptyPage, InvalidPage):
        noticias = paginator.page(paginator.num_pages)
    return render_to_response('capitalteresina/noticias/listagem.html', locals(),
                              context_instance=RequestContext(request))

def ver_secao(request, categoria):
    try:
        publicidades_3 = Publicidade.objects.filter(posicao = 4,data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades_3 = []
    secao = Secao.objects.get(url = categoria)
    secoes_id = [secao.id]
    for s in secao.secao_set.all():
        secoes_id.append(s.id)
    destaques_id = []
    destaques = Materias.objects.filter(status=True, secao__id__in = secoes_id, foto__isnull = False).exclude(foto='').order_by('-publicado_em')[:2]
    for destaque in destaques:
        destaques_id.append(destaque.id)
    noticias = Materias.objects.filter(status=True, secao__id__in=secoes_id).exclude(publicado_em__gte=datetime.datetime.today()).exclude(id__in = destaques_id).order_by('-publicado_em')
    secoes = Secao.objects.filter(status = True)
    ultimas = Materias.objects.filter(status = True).exclude(publicado_em__gte=datetime.datetime.today())[:5]
    paginator = Paginator(noticias, 5)
    titulo = 'Artigos'
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        noticias = paginator.page(page)
    except (EmptyPage, InvalidPage):
        noticias = paginator.page(paginator.num_pages)
    return render_to_response('capitalteresina/noticias/listagem.html', locals(),
                              context_instance=RequestContext(request))

def busca(request):
    try:
        queryb = request.GET['q']
        if queryb:
            qset = (
                Q(titulo__icontains=queryb) |
                Q(subtitulo__icontains=queryb) |
                Q(texto__icontains=queryb)
            )
    except:
        try:
            queryb = request.GET['palavra']
            if queryb:
                qset = (
                    Q(titulo__icontains=queryb) |
                    Q(subtitulo__icontains=queryb) |
                    Q(texto__icontains=queryb)
                )
        except:
            queryb = None
    if queryb:
        noticias = Materias.objects.filter(qset)
    else:
        return render_to_response('404.html', locals(),
                              context_instance=RequestContext(request))
    flagbusca = True
    ultimas = Materias.objects.filter(status = True).exclude(publicado_em__gte=datetime.datetime.today())[:5]
    paginator = Paginator(noticias, 10)
    try:
        publicidades_3 = Publicidade.objects.filter(posicao = 4,data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades_3 = []
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        noticias = paginator.page(page)
    except (EmptyPage, InvalidPage):
        noticias = paginator.page(paginator.num_pages)

    return render_to_response('capitalteresina/noticias/listagem.html', locals(),
                              context_instance=RequestContext(request))

def colunas(request):
    try:
        publicidades_3 = Publicidade.objects.filter(posicao = 4,data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades_3 = []
    ultimas = Materias.objects.filter(status = True).exclude(publicado_em__gte=datetime.datetime.today())[:5]
    colunas = Coluna.objects.filter(secao__status = True).order_by('secao__nome')
    secoes = Secao.objects.filter(status = True)
    return render_to_response('capitalteresina/noticias/listagem_colunas.html', {'colunas' : colunas, 'secoes': secoes, 'ultimas' : ultimas, 'publicidades_3': publicidades_3},
                              context_instance = RequestContext(request))

def ver_coluna(request, secao):
    coluna = Coluna.objects.get(secao__url = secao)
    if coluna.tipo == 'Social':
        # last_publication = Nota.objects.filter(secao__url = secao).order_by('-publicado_em')[:1]
        # filter_data = last_publication[0].publicado_em
        # data_coluna = "%s/%s/%s" % (filter_data.day, filter_data.month, filter_data.year)
        # notas = Nota.objects.filter(secao__url=secao).filter(publicado_em__day=filter_data.day, publicado_em__month = filter_data.month, publicado_em__year = filter_data.year)
        notas = Nota.objects.filter(secao__url=secao).order_by('-publicado_em')
        # dias_anteriores = Nota.objects.exclude(publicado_em__day=filter_data.day, publicado_em__month = filter_data.month, publicado_em__year = filter_data.year)
        paginator = Paginator(notas, 10)
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        try:
            if notas:
                notas = paginator.page(page)
            else:
                notas = paginator.page(page+1)
        except (EmptyPage, InvalidPage):
            notas = paginator.page(paginator.num_pages)
    else:
        notas = Nota.objects.filter(secao__url=secao).exclude(publicado_em__gte=datetime.datetime.today()).order_by('-publicado_em')
        paginator = Paginator(notas, 4)
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1
        try:
            notas = paginator.page(page)
        except (EmptyPage, InvalidPage):
            notas = paginator.page(paginator.num_pages)
    ultimas = Materias.objects.filter(status = True).exclude(publicado_em__gte=datetime.datetime.today())[:5]
    secoes = Secao.objects.filter(status = True)
    biografia = True
    paginacao = True
    titulo = coluna.secao.nome
    try:
        publicidades_3 = Publicidade.objects.filter(posicao = 4,data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades_3 = []

    if coluna.tipo == 'Social':
        return render_to_response('capitalteresina/noticias/ver_coluna_social.html', locals(),
                                  context_instance=RequestContext(request))
    else:
        return render_to_response('capitalteresina/noticias/ver_coluna.html', locals(),
                                  context_instance=RequestContext(request))

def ver_nota(request, secao, slug, id):
    notas = Nota.objects.filter(secao__url=secao, slug = slug).exclude(publicado_em__gte=datetime.datetime.today()).order_by('-publicado_em')
    coluna = Coluna.objects.get(secao__url = secao)
    ultimas = Materias.objects.filter(status = True).exclude(publicado_em__gte=datetime.datetime.today())[:5]
    secoes = Secao.objects.filter(status = True)
    paginator = Paginator(notas, 10)
    social = True
    titulo = coluna.secao.nome
    try:
        publicidades_3 = Publicidade.objects.filter(posicao = 4,data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades_3 = []
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        notas = paginator.page(page)
    except (EmptyPage, InvalidPage):
        notas = paginator.page(paginator.num_pages)

    return render_to_response('capitalteresina/noticias/ver_coluna.html', locals(),
                              context_instance=RequestContext(request))


def infograficos(request):
    infograficos = Infografico.objects.all().order_by('-publicado_em')
    ultimas = Materias.objects.filter(status = True).exclude(publicado_em__gte=datetime.datetime.today())[:5]
    paginator = Paginator(infograficos, 4)
    try:
        publicidades_3 = Publicidade.objects.filter(posicao = 4,data_entrada__lte=datetime.datetime.now(),data_saida__gte=datetime.datetime.now()).order_by('?')[0]
    except:
        publicidades_3 = []
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        infograficos = paginator.page(page)
    except (EmptyPage, InvalidPage):
        infograficos = paginator.page(paginator.num_pages)
    return render_to_response('capitalteresina/infografico.html', locals(), context_instance=RequestContext(request))


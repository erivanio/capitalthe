# -*- coding: utf-8 -*-
from django.contrib import admin
from datetime import datetime
from image_cropping import ImageCroppingMixin
#models
from models import Secao, Materias, Frase, Coluna, Nota, Infografico
from apps.materias.forms import MateriasFormAdmin, NotaFormAdmin

class FraseInline(admin.TabularInline):
    model = Frase
    extra = 1


class MateriasAdmin(ImageCroppingMixin,admin.ModelAdmin):

    list_per_page = 50
    form = MateriasFormAdmin
    inlines = [FraseInline]
    fieldsets = [
                 (None, {'fields':[('titulo', 'subtitulo'), 'publicado_em', 'texto', 'fonte', 'autor','palavras_chaves']}),
                 ('Secao', {'fields':[('secao')]}),
                 ('Midia', {'fields':['foto', ('legenda', 'credito'),'foto_destaque', ('credito_destaque'), 'foto_destaque_materia', 'album']}),
                 ('Video', {'fields':[('ativarvideo', 'video')]}),
                 ('Comentarios', {'fields':['ativarcomentarios'], 'classes': ['collapse closed']}),
                 ('Mais Informacoes', {'fields':['status'], 'classes': ['collapse closed']}),
    ]
    list_display = ('titulo', 'secao', 'publicado_em', 'foto_thumbnail', 'criador', 'visualizacoes','get_actions')
    list_filter = ['publicado_em', 'ativarvideo', 'status', 'secao', 'criador']
    search_fields = ['titulo', 'subtitulo']
    date_hierarchy = 'publicado_em'
    def queryset(self, request):
        queryset = super(MateriasAdmin, self).queryset(request)
        try:
            q = queryset.filter(secao=request.user.get_profile().secao)
        except:
            q = queryset.filter()
        return q
    def save_model(self, request, obj, form, change):
        if not change:
            obj.cadastrado_em = datetime.today()
            obj.criador = request.user
        obj.alterador = request.user
        obj.alterado_em = datetime.today()
        try:
            obj.secao = request.user.get_profile().secao
        except:
            pass
        obj.save()
    
    def foto_thumbnail(self, obj):
        if obj.foto:
            return u'<img height="50px" src="/media/%s" />' % (obj.foto)
        else:
            return "Sem Imagem" 
    foto_thumbnail.short_description = 'Foto'
    foto_thumbnail.allow_tags = True    

    def get_actions(self, obj):
        try:
            if obj.id:
                return u'<a class="add-another" onclick="return showAddAnotherPopup(this);" href="/full128/destaques/destaque/add/?_popup=1&materia_id=%s">Add Destaque</a>\
                        <a target="blank" href="%s">Ver no site</a>' % (obj.id, obj.get_absolute_url())
        except:
            return
    get_actions.allow_tags = True
    get_actions.short_description = "Ações"

admin.site.register(Materias, MateriasAdmin)

class SecaoAdmin(admin.ModelAdmin):
    prepopulated_fields = {"url": ("nome",)}    

    list_display = ('nome', 'url', 'descricao', 'dt_cadastro', 'dt_ultAtualizacao', 'status')
    list_filter = ['dt_cadastro', 'dt_ultAtualizacao', 'status']
    search_fields = ['nome']
    date_hierarchy = 'dt_cadastro'

admin.site.register(Secao, SecaoAdmin)

class ColunaAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('secao', 'user')

admin.site.register(Coluna, ColunaAdmin)

class NotaAdmin(admin.ModelAdmin):
    form = NotaFormAdmin
    prepopulated_fields = {'slug': ('titulo',)}
    list_display = ('titulo','publicado_em','secao')
    list_filter = ['publicado_em','secao']
    fields = ('titulo', 'nota', 'secao', 'album','primeira_foto','legenda_primeira_foto', 'publicado_em', 'slug')

    def queryset(self, request):
        queryset = super(NotaAdmin, self).queryset(request)

        try:
            if request.user.is_superuser != 1:
                coluna = Coluna.objects.get(user = request.user)
                q = queryset.filter(secao = coluna.secao)
            else:
                q = queryset.filter()
        except:
            q = queryset.filter()
        return q

    def save_model(self, request, obj, form, change):
        if not change:
            obj.user = request.user
        try:
            coluna = Coluna.objects.get(user = request.user)
            obj.secao = coluna.secao
        except Exception,e:
            pass
        obj.save()


class InfograficoAdmin(admin.ModelAdmin):
    list_display = ('materia', 'publicado_em', 'get_img')
    #form = InfograficoFormAdmin

    def get_img(self, obj):
        return u'<img width="100" heigth="50" src="/media/%s"></img>' % obj.imagem
    get_img.allow_tags = True
    get_img.short_description = 'Imagem'


    class Meta:
        model = Infografico


admin.site.register(Nota, NotaAdmin)
admin.site.register(Infografico, InfograficoAdmin)
# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Secao'
        db.create_table(u'materias_secao', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=35)),
            ('url', self.gf('django.db.models.fields.SlugField')(max_length=35)),
            ('descricao', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('dt_cadastro', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('dt_ultAtualizacao', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('rodape', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('secao_origem', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['materias.Secao'], null=True, blank=True)),
        ))
        db.send_create_signal(u'materias', ['Secao'])

        # Adding model 'Materias'
        db.create_table(u'materias_materias', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('subtitulo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('texto', self.gf('redactor.fields.RedactorField')()),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200, blank=True)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200, blank=True)),
            ('album', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['multimidia.Album'], null=True, blank=True)),
            ('foto', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('foto_materia', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('legenda', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('credito', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('foto_destaque', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('foto_destaque_materia', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('legenda_destaque', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('credito_destaque', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('ativarvideo', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('video', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['multimidia.Video'], null=True, blank=True)),
            ('publicado_em', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('ativarcomentarios', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('secao', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['materias.Secao'])),
            ('visualizacoes', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('criador', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='criador', null=True, to=orm['auth.User'])),
            ('cadastrado_em', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('alterador', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='alterador', null=True, to=orm['auth.User'])),
            ('alterado_em', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('fonte', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('autor', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('palavras_chaves', self.gf('tagging.fields.TagField')()),
        ))
        db.send_create_signal(u'materias', ['Materias'])

        # Adding model 'Frase'
        db.create_table(u'materias_frase', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('materia', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['materias.Materias'])),
            ('autor', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('frase', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('paragrafo', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'materias', ['Frase'])

        # Adding model 'Coluna'
        db.create_table(u'materias_coluna', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('secao', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['materias.Secao'])),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('biografia', self.gf('django.db.models.fields.TextField')()),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('imagem', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('imagem_thumb', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('imagem_media', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('imagem_media_thumb', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('imagem_header', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('imagem_header_thumb', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal(u'materias', ['Coluna'])

        # Adding model 'Nota'
        db.create_table(u'materias_nota', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('nota', self.gf('redactor.fields.RedactorField')(null=True, blank=True)),
            ('primeira_foto', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('legenda_primeira_foto', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('segunda_foto', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('legenda_segunda_foto', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('secao', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['materias.Secao'])),
            ('album', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['multimidia.Album'], null=True, blank=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200)),
            ('publicado_em', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
            ('ordem', self.gf('django.db.models.fields.IntegerField')(default=99)),
        ))
        db.send_create_signal(u'materias', ['Nota'])

        # Adding model 'Infografico'
        db.create_table(u'materias_infografico', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('descricao', self.gf('django.db.models.fields.TextField')()),
            ('creditos', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('imagem', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('materia', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['materias.Materias'])),
            ('publicado_em', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 8, 27, 0, 0))),
        ))
        db.send_create_signal(u'materias', ['Infografico'])


    def backwards(self, orm):
        # Deleting model 'Secao'
        db.delete_table(u'materias_secao')

        # Deleting model 'Materias'
        db.delete_table(u'materias_materias')

        # Deleting model 'Frase'
        db.delete_table(u'materias_frase')

        # Deleting model 'Coluna'
        db.delete_table(u'materias_coluna')

        # Deleting model 'Nota'
        db.delete_table(u'materias_nota')

        # Deleting model 'Infografico'
        db.delete_table(u'materias_infografico')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'materias.coluna': {
            'Meta': {'object_name': 'Coluna'},
            'biografia': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagem': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'imagem_header': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'imagem_header_thumb': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'imagem_media': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'imagem_media_thumb': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'imagem_thumb': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'secao': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['materias.Secao']"}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'materias.frase': {
            'Meta': {'object_name': 'Frase'},
            'autor': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'frase': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'materia': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['materias.Materias']"}),
            'paragrafo': ('django.db.models.fields.IntegerField', [], {})
        },
        u'materias.infografico': {
            'Meta': {'object_name': 'Infografico'},
            'creditos': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'descricao': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagem': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'materia': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['materias.Materias']"}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 8, 27, 0, 0)'})
        },
        u'materias.materias': {
            'Meta': {'ordering': "('-publicado_em',)", 'object_name': 'Materias'},
            'album': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['multimidia.Album']", 'null': 'True', 'blank': 'True'}),
            'alterado_em': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'alterador': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'alterador'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'ativarcomentarios': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ativarvideo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'autor': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'credito': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'credito_destaque': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'criador': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'criador'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'fonte': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'foto_destaque': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'foto_destaque_materia': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'foto_materia': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legenda': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'legenda_destaque': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'palavras_chaves': ('tagging.fields.TagField', [], {}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'secao': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['materias.Secao']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'subtitulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'texto': ('redactor.fields.RedactorField', [], {}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'blank': 'True'}),
            'video': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['multimidia.Video']", 'null': 'True', 'blank': 'True'}),
            'visualizacoes': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'materias.nota': {
            'Meta': {'object_name': 'Nota'},
            'album': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['multimidia.Album']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legenda_primeira_foto': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'legenda_segunda_foto': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'nota': ('redactor.fields.RedactorField', [], {'null': 'True', 'blank': 'True'}),
            'ordem': ('django.db.models.fields.IntegerField', [], {'default': '99'}),
            'primeira_foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'secao': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['materias.Secao']"}),
            'segunda_foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        u'materias.secao': {
            'Meta': {'ordering': "('nome',)", 'object_name': 'Secao'},
            'descricao': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'dt_cadastro': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dt_ultAtualizacao': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '35'}),
            'rodape': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'secao_origem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['materias.Secao']", 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'url': ('django.db.models.fields.SlugField', [], {'max_length': '35'})
        },
        u'multimidia.album': {
            'Meta': {'ordering': "('-cadastrado_em',)", 'object_name': 'Album'},
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'creditos': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'foto_big': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'foto_destaque': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'foto_medium': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'foto_small': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'visualizacoes': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'multimidia.video': {
            'Meta': {'ordering': "('-publicado_em',)", 'object_name': 'Video'},
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'video': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'video_id': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'visualizacoes': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['materias']
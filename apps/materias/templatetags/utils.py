from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def trim(value):
	return value.strip()

@register.filter
@stringfilter
def truncatechars(value, limit = 100):
    if len(value) < limit:
        return value
    else:
        return value[:limit] + '...'
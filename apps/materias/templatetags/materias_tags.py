from django import template
from apps.materias.models import Materias

register = template.Library()

@register.simple_tag(takes_context=True)
def get_ultimas(context):
	from datetime import datetime 
	now = datetime.now()
	agora = datetime(now.year, now.month, now.day, now.hour, 0, 0, 0)
	context['ultimas'] = Materias.objects.filter(status = True).exclude(publicado_em__gte=agora).order_by('-publicado_em')[:4]
	return ''

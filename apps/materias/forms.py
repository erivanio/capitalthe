# -*- coding: utf-8 -*-
from django import forms
from models import Materias, Nota, Infografico
from redactor.widgets import RedactorEditor
from ckeditor.widgets import CKEditorWidget


class NotaFormAdmin(forms.ModelForm):
    class Meta:
        model = Nota
        widgets = {'nota': CKEditorWidget}


# class InfograficoFormAdmin(forms.ModelForm):
#
#     class Meta:
#         model = Infografico
#         widgets = {
#            'descricao': CKEditorWidget
#         }

class MateriasFormAdmin(forms.ModelForm):

    class Meta:
        model = Materias
        widgets = {
           'texto': CKEditorWidget
        }

    def clean_palavras_chaves(self):
         palavras_chaves = self.cleaned_data['palavras_chaves']
         if palavras_chaves=='':
             raise forms.ValidationError("Ao cadastrar uma noticia voce deve informar palavras chaves!")
         return palavras_chaves

    #def clean_fonte(self):
         #fonte = self.cleaned_data['fonte']
         #if fonte=='':
             #raise forms.ValidationError("Ao cadastrar uma noticia voce deve informar uma fonte!")
         #return fonte

    def clean_legenda(self):
         foto = self.cleaned_data['foto']
         legenda = self.cleaned_data['legenda']
         if foto:
             if legenda=='':
                 raise forms.ValidationError("Ao cadastrar uma foto voce deve informar uma legenda!")
         return legenda

    def clean_credito(self):
         foto = self.cleaned_data['foto']
         credito = self.cleaned_data['credito']
         if foto:
             if credito=='':
                 raise forms.ValidationError("Ao cadastrar uma foto voce deve informar uma legenda!")
         return credito

    def clean_credito_destaque(self):
         foto = self.cleaned_data['foto_destaque']
         credito = self.cleaned_data['credito_destaque']
         if foto:
             if credito=='':
                 raise forms.ValidationError("Ao cadastrar uma foto voce deve informar uma legenda!")
         return credito
    
    # def clean_video(self):
    #     ativarvideo = self.cleaned_data['ativarvideo']
    #     video = self.cleaned_data['video']
    #     if ativarvideo:
    #         if video=='':
    #             raise forms.ValidationError("A opcao 'Utilizar Video' est� ativa! Por Favor informe uma url de video!")
    #     return video
    
    # def clean_redir_link(self):
    #     redir = self.cleaned_data['redir']
    #     redir_link = self.cleaned_data['redir_link']
    #     if redir:
    #         if redir_link=='':
    #             raise forms.ValidationError("A opção 'Redirecionar' está ativa! Por Favor informe uma url para redirecionar!")
    #     return redir_link

class MateriasQuickForm(forms.ModelForm):
    class Meta:
        model = Materias
        fields = ('titulo','texto')

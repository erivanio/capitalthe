# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Contato.email'
        db.alter_column(u'agenda_contato', 'email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True))

        # Changing field 'Telefone.telefone'
        db.alter_column(u'agenda_telefone', 'telefone', self.gf('django.db.models.fields.CharField')(max_length=15))

    def backwards(self, orm):

        # Changing field 'Contato.email'
        db.alter_column(u'agenda_contato', 'email', self.gf('django.db.models.fields.EmailField')(default=None, max_length=75))

        # Changing field 'Telefone.telefone'
        db.alter_column(u'agenda_telefone', 'telefone', self.gf('django.db.models.fields.CharField')(max_length=14))

    models = {
        u'agenda.contato': {
            'Meta': {'object_name': 'Contato'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'empresa': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'agenda.telefone': {
            'Meta': {'object_name': 'Telefone'},
            'contato': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agenda.Contato']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'telefone': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }

    complete_apps = ['agenda']
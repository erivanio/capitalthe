# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Contato'
        db.create_table(u'agenda_contato', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('empresa', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('telefone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['agenda.Telefone'])),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
        ))
        db.send_create_signal(u'agenda', ['Contato'])

        # Adding model 'Telefone'
        db.create_table(u'agenda_telefone', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('telefone', self.gf('django.db.models.fields.CharField')(max_length=14)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal(u'agenda', ['Telefone'])


    def backwards(self, orm):
        # Deleting model 'Contato'
        db.delete_table(u'agenda_contato')

        # Deleting model 'Telefone'
        db.delete_table(u'agenda_telefone')


    models = {
        u'agenda.contato': {
            'Meta': {'object_name': 'Contato'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'empresa': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'telefone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agenda.Telefone']"})
        },
        u'agenda.telefone': {
            'Meta': {'object_name': 'Telefone'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'telefone': ('django.db.models.fields.CharField', [], {'max_length': '14'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }

    complete_apps = ['agenda']
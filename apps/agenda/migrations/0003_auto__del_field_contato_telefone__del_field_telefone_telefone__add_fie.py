# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Contato.telefone'
        db.delete_column(u'agenda_contato', 'telefone_id')

        # Deleting field 'Telefone.telefone'
        db.delete_column(u'agenda_telefone', 'telefone')

        # Adding field 'Telefone.contato'
        db.add_column(u'agenda_telefone', 'contato',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['agenda.Contato']),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Contato.telefone'
        db.add_column(u'agenda_contato', 'telefone',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['agenda.Telefone']),
                      keep_default=False)

        # Adding field 'Telefone.telefone'
        db.add_column(u'agenda_telefone', 'telefone',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=14),
                      keep_default=False)

        # Deleting field 'Telefone.contato'
        db.delete_column(u'agenda_telefone', 'contato_id')


    models = {
        u'agenda.contato': {
            'Meta': {'object_name': 'Contato'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'empresa': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'agenda.telefone': {
            'Meta': {'object_name': 'Telefone'},
            'contato': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agenda.Contato']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }

    complete_apps = ['agenda']
# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.agenda.models import Contato, Telefone
from capitalthe import settings

class TelefoneInline(admin.TabularInline):
    model = Telefone
    extra = 1


class ContatoAdmin(admin.ModelAdmin):
    #class Media:
    #    js = ('%sjs/mascara.js' % settings.STATIC_URL,'%sjs/maskedinput.js' % settings.STATIC_URL)
    inlines = [TelefoneInline]
    list_display = ('nome','empresa', 'email','telefone',)
    list_filer = ['empresa']
    search_fields = ['nome', 'empresa', 'email']

    def telefone(self, obj):
        return "%s" % (' , '.join(telefone.telefone for telefone in obj.telefone_set.all()))

admin.site.register(Contato, ContatoAdmin)

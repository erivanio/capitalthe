# -*- coding: utf-8 -*-
from django.db import models
POSICAO_CHOICES = (('1', 'Residencial'), ('2', 'Empresa'), ('3', 'Celular'))

class Contato(models.Model):
    nome = models.CharField(max_length=50)
    empresa = models.CharField('Empresa / Órgão', max_length=50, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)

    def __unicode__(self):
        return self.nome

class Telefone(models.Model):
    contato=models.ForeignKey('Contato')
    tipo=models.CharField('Tipo', max_length=1, choices = POSICAO_CHOICES)
    telefone=models.CharField('Telefone', max_length=15)
    def __unicode__(self):
        return self.telefone

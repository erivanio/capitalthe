# -*- encoding: utf-8 -*-
from django.db import models
from apps.materias.models import Secao


class WebSite(models.Model):
    nome = models.CharField(max_length=100, help_text='Nome do site')
    site = models.CharField(max_length=100, help_text='Endereço do site')
    path = models.CharField(max_length=150, help_text='XPath do link da matéria', blank=True, null=True)
    path_titulo = models.CharField(max_length=150, help_text='XPath do título da matéria', blank=True, null=True)
    path_subtitulo = models.CharField(max_length=150, help_text='XPath do subtítulo da matéria', blank=True, null=True)
    path_texto = models.CharField(max_length=150, help_text='XPath do texto da matéria', blank=True, null=True)
    path_categoria = models.CharField(max_length=150, blank=True, null=True, help_text='XPath da categoria da matéria')
    path_data = models.CharField(max_length=150, blank=True, null=True, help_text='XPath da data de publicacao')
    formato_data = models.CharField(max_length=20, blank=True, null=True, help_text='Formato da data de publicacao')
    start_page = models.CharField(max_length=150, help_text='Página onde será iniciada a busca', blank=True, null=True)
    domains = models.CharField(max_length=200, help_text='Domínios onde serão permitidas as buscas', blank=True, null=True)

    def __unicode__(self):
        return self.nome

    class Meta:
        verbose_name = 'WebSite'
        verbose_name_plural = 'WebSites'


class Editorias(models.Model):
    site = models.ForeignKey(WebSite)
    editoria_capital = models.ForeignKey(Secao)
    editoria_site = models.CharField(max_length = 100)

    def __unicode__(self):
        return "%s - %s" % (self.editoria_site, self.editoria_capital)
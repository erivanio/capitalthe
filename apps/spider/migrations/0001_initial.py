# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'WebSite'
        db.create_table(u'spider_website', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('site', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('path', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('path_titulo', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('path_subtitulo', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('path_texto', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('path_categoria', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('path_data', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('formato_data', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('start_page', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('domains', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'spider', ['WebSite'])

        # Adding model 'Editorias'
        db.create_table(u'spider_editorias', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('site', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['spider.WebSite'])),
            ('editoria_capital', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['materias.Secao'])),
            ('editoria_site', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'spider', ['Editorias'])


    def backwards(self, orm):
        # Deleting model 'WebSite'
        db.delete_table(u'spider_website')

        # Deleting model 'Editorias'
        db.delete_table(u'spider_editorias')


    models = {
        u'materias.secao': {
            'Meta': {'ordering': "('nome',)", 'object_name': 'Secao'},
            'descricao': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'dt_cadastro': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dt_ultAtualizacao': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '35'}),
            'rodape': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'secao_origem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['materias.Secao']", 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'url': ('django.db.models.fields.SlugField', [], {'max_length': '35'})
        },
        u'spider.editorias': {
            'Meta': {'object_name': 'Editorias'},
            'editoria_capital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['materias.Secao']"}),
            'editoria_site': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['spider.WebSite']"})
        },
        u'spider.website': {
            'Meta': {'object_name': 'WebSite'},
            'domains': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'formato_data': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'path_categoria': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'path_data': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'path_subtitulo': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'path_texto': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'path_titulo': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'start_page': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['spider']
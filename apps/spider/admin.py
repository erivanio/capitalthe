from django.contrib import admin
from .models import WebSite, Editorias

class EditoriasAdmin(admin.ModelAdmin):

	list_display = ('editoria_site', 'editoria_capital', 'site')
	list_filter = ['site', 'editoria_capital']
	search_fields = ['site', 'editoria_capital']

admin.site.register(WebSite)
admin.site.register(Editorias, EditoriasAdmin)
from django.conf.urls import patterns, include, url
from capitalthe import settings
from django.http import HttpResponse
from django.contrib import admin
from apps.multimidia.views import PictureCreateView, PictureDeleteView
from django.views.generic import RedirectView
from django.contrib.auth.decorators import login_required
from django.contrib.sitemaps import Sitemap, FlatPageSitemap
from apps.materias.models import Materias
from apps.materias.feeds import UltimasNoticias, UltimasNotas

sitemaps = {
    'site': Sitemap,
    'flatpages': FlatPageSitemap,
    }

admin.autodiscover()

urlpatterns = patterns('',
     #index
     (r'^$', 'capitalthe.views.home'),
     (r'^index.php/$', 'capitalthe.views.home'),
     #(r'^admin/', include(admin.site.urls)),
     (r'^contato/$', 'capitalthe.views.contato'),
     url(r'^rss/ultimasnoticias/', UltimasNoticias()),
     url(r'^rss/ultimasnotas/', UltimasNotas()),
     # (r'^cadastrar/email/$', 'capitalteresina.contatos.views.cadastrar_email'),
     # (r'^cadastrar/sms/$', 'capitalteresina.contatos.views.cadastrar_sms'),
     #('^pages/', include('django.contrib.flatpages.urls'),

     #noticias e artigos
     (r'^noticias/$', 'apps.materias.views.noticias'),
     (r'^noticias/(?P<categoria>[\w_-]+)/(?P<slug>[\w_-]+)-(?P<id>\d+).html', 'apps.materias.views.ver_materia'),
     (r'^noticias/imprimir/(?P<categoria>[\w_-]+)/(?P<slug>[\w_-]+)-(?P<id>\d+).html', 'apps.materias.views.imprimir_materia'),
     (r'^noticias/(?P<categoria>[\w_-]+)/', 'apps.materias.views.ver_secao'),

     #colunas
     (r'^colunas/(?P<secao>[\w_-]+)/(?P<slug>[\w_-]+)-(?P<id>\d+).html', 'apps.materias.views.ver_nota'),
     (r'^colunas/(?P<secao>[\w_-]+)/', 'apps.materias.views.ver_coluna'),
     (r'^colunas/$', 'apps.materias.views.colunas'),

     #albuns
     (r'^albuns/$', 'apps.multimidia.views.central_albuns'),
     (r'^albuns/(?P<slug>[\w_-]+)-(?P<id>\d+).html', 'apps.multimidia.views.album'),
     #videos
     # (r'^videos/$', 'capitalteresina.multimidia.views.central_videos'),
     # (r'^videos/(?P<slug>[\w_-]+)-(?P<id>\d+).html', 'capitalteresina.multimidia.views.video'),
     # (r'^cultura/cinema/$', 'capitalteresina.cinema.views.cinema'),
     #jornal
     (r'^jornal/$', 'apps.jornal.views.jornal'),

     #busca
     (r'^busca/$', 'apps.materias.views.busca'),
     (r'^infograficos/$', 'apps.materias.views.infograficos'),

     #cinema
     (r'^cinema/$', 'apps.cinema.views.cinema'),
     (r'^filme/(?P<slug>[\w_-]+)', RedirectView.as_view(url='/cinema/%(slug)s/')),
     (r'^cinema/(?P<slug>[\w_-]+)', 'apps.cinema.views.filme'),

     url(r'^full128/', include(admin.site.urls)),
     url(r'^redactor/', include('redactor.urls')),
     (r'^ckeditor/', include('ckeditor.urls')),
     # (r'^adzone/', include('capitalteresina.publicidade.urls')),

     (r'^robots\.txt$', lambda r: HttpResponse("User-agent: *\nAllow: /", mimetype="text/plain")),
     (r'^sitemap\.xml$', 'capitalthe.views.sitemap'),
     (r'^sitemap-news\.xml$', 'capitalthe.views.sitemap_gnews'),

     # (r'^multimidia/foto/multiupload/$', 'apps.multimidia.views.album_id'),
     #(r'^multimidia/foto/multiupload/(?P<album_id>\d+)/$', 'apps.multimidia.views.view'),
     # (r'^multimidia/foto/multiupload/(?P<album_id>\d+)/$', 'apps.multimidia.views.album_id',),

     (r'^nup/new/(?P<album_id>\d+)/$', login_required(PictureCreateView.as_view()), {}, 'upload-new'),
     (r'^nup/delete/(?P<pk>\d+)$', login_required(PictureDeleteView.as_view()), {}, 'upload-delete'),
     ('^pages/', include('django.contrib.flatpages.urls')),
)

urlpatterns += patterns('',
    (r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'^static/(.*)$', 'django.views.static.serve', {'document_root' : settings.STATIC_ROOT}),
)
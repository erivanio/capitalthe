# -*- coding: utf-8 -*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import Http404, HttpResponseRedirect
from django.core.cache import cache
from django.db.models import Q
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django import forms
from django.contrib import messages
from django.core.mail import send_mail

import sys
import time
from datetime import datetime

#models
from apps.materias.models import Materias, Coluna, Nota
from tagging.models import TaggedItem, Tag
from apps.multimidia.models import Album
from apps.destaques.models import Destaque
from datetime import datetime
from apps.publicidade.models import Publicidade



def home(request):
    try:
        destaques = Destaque.objects.filter(status=True, posicao=1, publicado_em__lte=datetime.today()).order_by('-publicado_em')[:4]
    except Exception,e:
        destaques = []
    try:
        destaque_secundario = Destaque.objects.filter(status=True, posicao=2, publicado_em__lte=datetime.today()).order_by('-publicado_em')[0]
    except:
        destaque_secundario = []
    try:
        destaque_3 = Destaque.objects.filter(status=True, posicao=3, publicado_em__lte=datetime.today()).order_by('-publicado_em')[:2]
    except:
        destaque_3 = []
    try:
        destaque_4 = Destaque.objects.filter(status=True, posicao=4, publicado_em__lte=datetime.today()).order_by('-publicado_em')[0]
    except:
        destaque_4 = []
    try:
        destaque_5 = Destaque.objects.filter(status=True, posicao=5, publicado_em__lte=datetime.today()).order_by('-publicado_em')[0]
    except:
        destaque_5 = []
    try:
        destaque_6 = Destaque.objects.filter(status=True, posicao=6, publicado_em__lte=datetime.today()).order_by('-publicado_em')[0]
    except:
        destaque_6 = []
    try:
        destaque_7 = Destaque.objects.filter(status=True, posicao=7, publicado_em__lte=datetime.today()).order_by('-publicado_em')[0]
    except:
        destaque_7 = []
    try:
        destaque_8 = Destaque.objects.filter(status=True, posicao=8, publicado_em__lte=datetime.today()).order_by('-publicado_em')[0]
    except:
        destaque_8 = []
    try:
        destaque_9 = Destaque.objects.filter(status=True, posicao=9, publicado_em__lte=datetime.today()).order_by('-publicado_em')[0]
    except:
        destaque_9 = []
    try:
        destaque_10 = Destaque.objects.filter(status=True, posicao=10, publicado_em__lte=datetime.today()).order_by('-publicado_em')[0]
    except:
        destaque_10 = []
    try:
        destaque_11 = Destaque.objects.filter(status=True, posicao=11, publicado_em__lte=datetime.today()).order_by('-publicado_em')[:4]
    except:
        destaque_11 = []
    try:
        destaque_12 = Destaque.objects.filter(status=True, posicao=12, publicado_em__lte=datetime.today()).order_by('-publicado_em')[0]
    except:
        destaque_12 = []
    try:
        destaque_13 = Destaque.objects.filter(status=True, posicao=13, publicado_em__lte=datetime.today()).order_by('-publicado_em')[0]
    except:
        destaque_13 = []
    try:
        destaque_14 = Destaque.objects.filter(status=True, posicao=14, publicado_em__lte=datetime.today()).order_by('-publicado_em')[0]
    except:
        destaque_14 = []
    try:
        destaque_15 = Destaque.objects.filter(status=True, posicao=15, publicado_em__lte=datetime.today()).order_by('-publicado_em')[0]
    except:
        destaque_15 = []
    try:
        publicidades = Publicidade.objects.filter(posicao=1, data_entrada__lte=datetime.now(),data_saida__gte=datetime.now()).order_by('?')[0]
    except:
        publicidades = []
    try:
        publicidades_1 = Publicidade.objects.filter(posicao=2, data_entrada__lte=datetime.now(),data_saida__gte=datetime.now()).order_by('?')[0]
    except:
        publicidades_1 = []
    try:
        publicidades_2 = Publicidade.objects.filter(posicao=3, data_entrada__lte=datetime.now(),data_saida__gte=datetime.now()).order_by('?')[0]
    except:
        publicidades_2 = []

    colunas = Coluna.objects.all().order_by('?')[:6]
    albuns = Album.objects.filter(status=True).order_by('-publicado_em')[:7]
    charge = Nota.objects.filter(secao__url='dino-alves').order_by('-publicado_em')[:1]
    # from datetime import datetime
    # now = datetime.now()
    # agora = datetime(now.year, now.month, now.day, now.hour, 0, 0, 0)
    ultimas = Materias.objects.filter(status=True).exclude(publicado_em__gte=datetime.today())[:5]
    return render_to_response('capitalteresina/index.html', locals(),
                              context_instance=RequestContext(request))

class FormContato(forms.Form):
    nome = forms.CharField(max_length=50)
    email = forms.EmailField(required=True)
    setor = forms.CharField(max_length=80)
    mensagem = forms.Field(widget=forms.Textarea)

    def enviar(self):
        titulo = 'Capital Teresina - Mensagem enviada pelo site - %(setor)s' % self.cleaned_data
        destino = 'redacao@capitalteresina.com.br'
        texto = """
        Nome: %(nome)s
        E-mail: %(email)s
        Setor: %(setor)s
        Mensagem:
        %(mensagem)s
        """ % self.cleaned_data

        send_mail(
            subject = titulo,
            message = texto,
            from_email = destino,
            recipient_list = [destino],
            )

def contato(request):
    if request.method == 'POST':
        form = FormContato(request.POST)
        if form.is_valid():
            form.enviar()
            messages.success(request, 'Mensagem enviada com sucesso!')
    else:
        form = FormContato()
    return render_to_response('capitalteresina/contato.html', locals(), context_instance=RequestContext(request),)

def sitemap(request):
    p = request.GET.get('p',1)
    vmin = ((500 * int(p)) - 500)
    vmax = 500 * int(p)

    stmapgr = Materias.objects.filter(status=True).order_by('-id')[vmin:vmax]

    return render_to_response('xml/geral.xml', locals(),
                              context_instance=RequestContext(request))

def sitemap_gnews(request):

    gnews = Materias.objects.filter(status=True).order_by('-id')[:500]

    return render_to_response('xml/google-news.xml', locals(),
                              context_instance=RequestContext(request))
